# My Project Management Tool

In an effort to be more agile, I'm going to start practicing using boards to open and close action items, even really small ones.

I want to try to put into practice an agile approach to work, where items are moved from backlog, to in progress, to done. This project is an attempt at that.
